import app from './src/server';
import { AppDataSource } from "./src/db"
import { AppObjectStorage } from './src/storage'

AppDataSource.initialize()
    .then(() => {
        console.log('[OK] DB is connected');
    })
    .catch((err) => {
        console.error('[ERROR] DB isn\'t connected');
        console.error(err.message);
        process.exit(1);
    })

AppObjectStorage.connect()
    .then(_ => {
        console.log('[OK] Storage is connected')
    })
    .catch(e => {
        console.log('[ERROR] Storage is not connected')
        console.log(e.mesage)
    })

app.listen(app.get('port'), () => {
    console.log(`[OK] Server is running on ${app.get('port')} port`);
});