import { Entity, Column, BaseEntity, PrimaryColumn } from "typeorm";


@Entity()
export class UsersInfo extends BaseEntity {

    @PrimaryColumn()
    user_id: number;

    @Column({ default: '' })
    login: string;

    @Column({ default: '' })
    name: string;

    @Column({ default: '' })
    lastname: string;

    @Column({ default: null })
    birthday: Date;

    @Column({ default: '' })
    email: string;
}
