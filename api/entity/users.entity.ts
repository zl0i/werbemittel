import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToOne } from "typeorm";
import { UsersInfo } from "./users_info.entity";


@Entity()
export class Users extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    login: string

    @Column({ default: null })
    password: string;

    @Column()
    tier: string

    @Column()
    role: string

    @OneToOne(() => UsersInfo)
    info: UsersInfo
}