import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";


@Entity()
export class Tokens extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_id: number

    @Column()
    accessToken: string;

    @Column()
    refreshToken: string

    @Column({default: ""})
    type: string
}