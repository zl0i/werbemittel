import { Entity, Column, BaseEntity, PrimaryGeneratedColumn } from "typeorm";


export enum OAuthServices {
    VK = "vk",
    YA = "ya",
    GO = 'go'
}

@Entity()
export class OAuthUsers extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    id_user: number;

    @Column()
    oauth_id: string;

    @Column()
    access_token: string;

    @Column()
    refresh_token: string;

    @Column()
    service: OAuthServices
}