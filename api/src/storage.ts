import * as Minio from 'minio'

const MINIO_HOST = process.env['MINIO_HOST'] ?? 'minio'
const MINIO_USER = process.env["MINIO_USER"] ?? 'werbemittel'
const MINIO_PASSWORD = process.env["MINIO_PASSWORD"] ?? 'werbemittel'

class ObjectStorage {

    private minioClient: Minio.Client
    private options: Minio.ClientOptions;

    constructor(options: Minio.ClientOptions) {
        this.options = options
    }

    async connect() {
        return new Promise((resolve, reject) => {
            this.minioClient = new Minio.Client(this.options)
            this.minioClient.bucketExists('werbemittel', (err, res) => {
                if (err)
                    reject(err)

                if (res == false) {
                    this.minioClient.makeBucket('werbemittel', '')
                        .then((_val) => {
                            resolve(res)
                        })
                        .catch((err) => {
                            reject(err)
                        })
                } else {
                    resolve(res)
                }
            })
        })
    }
}

export const AppObjectStorage = new ObjectStorage({
    endPoint: MINIO_HOST,
    port: 9000,
    useSSL: false,
    accessKey: MINIO_USER,
    secretKey: MINIO_PASSWORD
})