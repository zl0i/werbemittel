import express from 'express'
import morgan from 'morgan';
import fileUpload from 'express-fileupload'
import authRouter from "../routers/auth.router"
import cookieParser from 'cookie-parser';

const app = express();

app.set('port', 3000);
app.use(express.json());
app.use(cookieParser());
app.use(fileUpload());
app.use(express.urlencoded({ extended: false }));
app.use(morgan(':date[iso] :remote-addr :method :url :status :response-time ms'));

app.use('/auth', authRouter)

export default app