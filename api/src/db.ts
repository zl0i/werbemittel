import "reflect-metadata";
import { DataSource } from "typeorm";

const DB_HOST: string = process.env['DB_HOST'] ?? 'localhost'
const DB_USER = process.env['DB_USER'] ?? 'postgres'
const DB_PASSWORD: string = process.env['DB_PASSWORD'] ?? 'admin';
const DB_NAME: string = process.env['DB_NAME'] ?? 'werbmittel'

if (process.env['NODE_ENV'] == 'dev') {
    console.log('[WARNING] This app started in dev mode!')
}

export const AppDataSource = new DataSource({
    type: "postgres",
    host: DB_HOST,
    port: 5432,
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    entities: [
        "./entity/*[.ts]"
    ],
    migrations: [
        "./entity/migrations/*[.ts]"
    ],
    migrationsRun: true,
    synchronize: true
})
