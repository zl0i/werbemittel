import axios from 'axios';
import { Users } from '../entity/users.entity';
import { Tokens } from '../entity/tokens.entity';
import { OAuthServices, OAuthUsers } from '../entity/oauth_users.entity';
import { UsersInfo } from '../entity/users_info.entity';
import jwt from "jsonwebtoken"

const redirect_uri = "https://zloi.space";

const vk_client_id = "8132816";
const vk_client_secret = process.env['VK_CLIENT_SECRET'];

const SECRET_KEY = process.env["SECRET"] ?? "tssss"

interface IUserInfo {
    id: string
    access_token: string,
    refresh_token: string
    token_expired: number,

    login: string,
    name: string
    lastname: string,
    birthday: Date,
    phone: string,
    email: string
}

export default class OAuthService {

    static redirectUser(method: string, device: string) {
        let url: string;
        switch (method) {
            case "vk":
                url = `https://oauth.vk.com/authorize?client_id=${vk_client_id}&display=page&scope=offline,groups,stats&response_type=code&v=5.131&state=vk`
                break;
            default: {
                return `${redirect_uri}?error=method_undefined`
            }
        }
        switch (device) {
            case "web":
                url = url + `&redirect_uri=${redirect_uri}`
                break;
            default: {
                return `${redirect_uri}?error=device_undefined`
            }
        }
        return url;
    }

    static async handleCode(code: string, state: string) {
        try {
            const info = await OAuthService.requstInfoByPlatform(code, state);

            const oauth_user = await OAuthUsers.findOne({ where: { oauth_id: info.id, service: state.toUpperCase() as OAuthServices } })
            const user = oauth_user
                ? (await Users.findOne({ where: { id: oauth_user.id_user } }) ?? new Users())
                : new Users()


            user.info = oauth_user ? await UsersInfo.findOne({ where: { user_id: oauth_user.id_user } }) ?? new UsersInfo() : new UsersInfo()
            user.info.login = info.login
            user.info.name = info.name;
            user.info.lastname = info.lastname
            user.info.birthday = info.birthday
            await user.save()
            if (!oauth_user) {
                await OAuthUsers.insert({
                    oauth_id: info.id,
                    id_user: user.id,
                    refresh_token: info.refresh_token,
                    access_token: info.access_token,
                    service: state as OAuthServices
                })
            }

            const token = new Tokens()
            token.user_id = user.id
            token.accessToken = jwt.sign({ id: user.id }, SECRET_KEY, { expiresIn: '15m' })
            token.refreshToken = jwt.sign({ id: user.id }, SECRET_KEY, { expiresIn: '10d' })
            await token.save()
            
            return `${redirect_uri}?accessToken=${token.accessToken}&refreshToken=${token.refreshToken}`
        } catch (error: any) {
            return `${redirect_uri}?message=${error.message}`
        }

    }

    static async requstInfoByPlatform(code: string, state: string,) {
        switch (state) {
            case 'vk':
                return await OAuthService.requstInfoVk(code)
                break;
            case 'df':
                return OAuthService.defaultInfo()
                break;
            default:
                throw new Error("state_error")
        }
    }

    static async requstInfoVk(code: string): Promise<IUserInfo> {
        const token = await axios.get('https://oauth.vk.com/access_token', {
            params: {
                client_id: vk_client_id,
                client_secret: vk_client_secret,
                redirect_uri: redirect_uri,
                code: code
            }
        });
        const info = await axios.get('https://api.vk.com/method/users.get', {
            params: {
                user_id: token.data.user_id,
                filter: 'editor',
                access_token: token.data.access_token,
                v: '5.131'
            }
        });
        return {
            id: token.data.user_id,
            access_token: token.data.access_token,
            refresh_token: "",
            token_expired: -1,

            login: info.data.response[0].domain,
            name: info.data.response[0].first_name,
            lastname: info.data.response[0].last_name,
            birthday: new Date(info.data.response[0].bdate),
            phone: info.data.response[0].phone,
            email: ""
        }
    }

    static defaultInfo(): IUserInfo {
        return {
            id: "694225478",
            access_token: "example_access_token",
            refresh_token: "example_refresh_token",
            token_expired: -1,

            login: 'example_login',
            name: 'example_name',
            lastname: 'example_lastname',
            birthday: new Date(),
            email: 'example@mail.com',
            phone: '+79999999999'
        }
    }
}