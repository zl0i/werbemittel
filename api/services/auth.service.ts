import { Users } from "../entity/users.entity"
import bcrypt from 'bcryptjs'
import jwt, { JwtPayload } from "jsonwebtoken"
import { Tokens } from "../entity/tokens.entity"
import { UnauthorizedError } from "../lib/httpErrorHandler"

const SECRET_KEY = process.env["SECRET"] ?? "tssss"


export default class AuthService {

    static async signUp(name: string, password: string) {
        const user = new Users()
        user.login = name
        user.password = bcrypt.hashSync(password, 5)
        user.tier = ""
        user.role = ""
        await user.save()
        const tokens = new Tokens()
        tokens.user_id = user.id
        tokens.refreshToken = jwt.sign({ id: user.id }, SECRET_KEY, { expiresIn: '10d' })
        tokens.accessToken = jwt.sign({ id: user.id }, SECRET_KEY, { expiresIn: '15m' })
        await tokens.save()
        return {
            accessToken: tokens.accessToken,
            refreshToken: tokens.refreshToken
        }
    }

    static async signIn(login: string, password: string) {
        const user = await Users.findOne({ where: { login } })
        if (!user) {
            throw new UnauthorizedError("User invalid")
        }
        if (!bcrypt.compareSync(password, user.password)) {
            throw new UnauthorizedError("Login or password is bad")
        }
        const row = await Tokens.findOne({ where: { user_id: user.id } }) ?? new Tokens()
        row.user_id = user.id
        row.accessToken = jwt.sign({ id: user.id }, SECRET_KEY, { expiresIn: '15m' })
        row.refreshToken = jwt.sign({ id: user.id }, SECRET_KEY, { expiresIn: '10d' })
        await row.save()
        return {
            accessToken: row.accessToken,
            refreshToken: row.refreshToken
        }
    }

    static async refreshToken(refreshToken: string) {
        const data = jwt.verify(refreshToken, SECRET_KEY) as JwtPayload
        const row = await Tokens.findOne({ where: { refreshToken: refreshToken, user_id: data['id'] } })
        if (refreshToken == row?.refreshToken) {
            delete data.exp
            delete data.iat
            row.accessToken = jwt.sign(data, SECRET_KEY, { expiresIn: '15m' })
            row.refreshToken = jwt.sign(data, SECRET_KEY, { expiresIn: '10d' })
            await row.save()
            return {
                accessToken: row.accessToken,
                refreshToken: row.refreshToken
            }
        } else {
            throw new Error("Token is bad")
        }
    }
}