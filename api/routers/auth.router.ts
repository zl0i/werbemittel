import express from 'express';
import HttpErrorHandler, { UnauthorizedError } from '../lib/httpErrorHandler';
import AuthService from '../services/auth.service';

const router = express.Router();

router.post('/signin', async (req: express.Request, res: express.Response) => {
    try {
        const { login, password } = req.body
        const data = await AuthService.signIn(login, password)
        res.cookie('refreshToken', data.refreshToken, {maxAge: 10 * 24 * 60 * 60 * 1000, httpOnly: true})
        res.json({accessToken: data.accessToken})
    } catch (error) {
        HttpErrorHandler.handle(error, res)
    }
})

router.post('/signup', async (req: express.Request, res: express.Response) => {
    try {
        const { login, password } = req.body
        const data = await AuthService.signUp(login, password)
        res.cookie('refreshToken', data.refreshToken, {maxAge: 10 * 24 * 60 * 60 * 1000, httpOnly: true})
        res.json({accessToken: data.accessToken})
    } catch (error) {
        HttpErrorHandler.handle(error, res)
    }
})

router.post('/findrefreshtoken', async (req: express.Request, res: express.Response) => {
    try {
        if (!req.cookies.refreshToken) {
            throw new UnauthorizedError("refresh token not found")
        }
        res.json({refreshToken: req.cookies.refreshToken})
    } catch (error) {
        HttpErrorHandler.handle(error, res)
    }
})

router.post('/refresh', async (req: express.Request, res: express.Response) => {
    try {
        const refreshToken = req.cookies.refreshToken
        const data = await AuthService.refreshToken(refreshToken)
        res.cookie('refreshToken', data.refreshToken, {maxAge: 10 * 24 * 60 * 60 * 1000, httpOnly: true})
        res.json({accessToken: data.accessToken})
    } catch (error) {
        HttpErrorHandler.handle(error, res)
    }
})

router.post('/logout', async (_req: express.Request, res: express.Response) => {
    res.clearCookie('refreshToken')
    res.json({message: "Logout completed"})
})

export default router