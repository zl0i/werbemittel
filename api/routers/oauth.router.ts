import express from 'express'
import HttpErrorHandler from '../lib/httpErrorHandler'
import OAuthService from '../services/oauth.service'

const router = express.Router()

router.get('/', (req: express.Request, res: express.Response) => {
    try {
        const method = req.query['method'] as string
        const device = req.query['device'] as string
        res.redirect(OAuthService.redirectUser(method, device))
    } catch (error) {
        HttpErrorHandler.handle(error, res)
    }
})


export default router