{{/*
Expand the name of the chart.
*/}}
{{- define "werbmittel.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Define host 
*/}}
{{- define "werbmittel.host" -}}
{{- if or (eq .Values.branch "master") (eq .Values.branch "main")  }}
{{- .Values.baseHost }}
{{- else if eq .Values.branch "stage"  }}
{{- printf "%s.%s" "stage" .Values.baseHost }}
{{- else if eq .Values.branch "dev" }}
{{- printf "%s.%s" "dev" .Values.baseHost }}
{{ else }}
{{- printf "%s.%s.%s" .Values.branch "dev" .Values.baseHost }}
{{- end }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "werbmittel.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "werbmittel.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "werbmittel.labels" -}}
helm.sh/chart: {{ include "werbmittel.chart" . }}
{{ include "werbmittel.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "werbmittel.selectorLabels" -}}
app.kubernetes.io/name: {{ include "werbmittel.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "werbmittel.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "werbmittel.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}