import { makeAutoObservable } from "mobx";

export interface IProject {
  title: string;
  socialNetworkUse?: string[];
}

class projectsStore {
  _projects: IProject[] = [];
  _selectedProject: IProject | undefined = undefined;
  constructor() {
    makeAutoObservable(this);
  }

  loadAllProjects(data: IProject[]) {
    this._projects = data;
  }

  addProject(data: IProject) {
    this._projects.push(data);
  }

  getProjects(): IProject[] {
    return this._projects;
  }

  setSelectedProject(project: IProject) {
    this._selectedProject = project;
  }

  getSelectedProject(): IProject | undefined {
    return this._selectedProject;
  }
}

export default new projectsStore();
