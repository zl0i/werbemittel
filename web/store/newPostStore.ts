import { makeAutoObservable } from "mobx";

class newPostStore {
  _isShowAddPictureDialog: boolean = false;
  _userPictures: File[] = [];

  constructor() {
    makeAutoObservable(this);
  }

  setShowAddPictureDialog(newValue: boolean) {
    this._isShowAddPictureDialog = newValue;
  }

  isShowAddPictureDialog(): boolean {
    return this._isShowAddPictureDialog;
  }

  addPicture(image: File) {
    this._userPictures.push(image);
  }

  removePictureById(id: number) {
    this._userPictures = this._userPictures.filter((e, ind) => ind !== id);
  }

  getPictures(): File[] {
    return this._userPictures;
  }
}

export default new newPostStore();
