import { makeAutoObservable } from "mobx";

class LoginStore {
  _mail: string = "";
  _accessToken: string = "";
  constructor() {
    makeAutoObservable(this);
  }

  setMail(input: string) {
    this._mail = input;
  }

  setUser(accessToken: string) {
    localStorage.setItem("accessToken", accessToken);
    this._accessToken = accessToken;
  }

  removeUser() {
    localStorage.removeItem("accessToken");
    this._accessToken = "";
  }

  getMail() {
    return this._mail;
  }

  getAccessToken() {
    return this._accessToken;
  }
}

export default new LoginStore();
