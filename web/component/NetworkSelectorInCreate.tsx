import { useRouter } from "next/router";
import { FC, useState } from "react";
import Image from "next/image";

import s from "../styles/createPost.module.css";

import { ISendTo } from "../pages/project/[projectName]/create";

const NetworkSelector: FC<{ data: ISendTo[] }> = (props) => {
  const router = useRouter();
  const { projectName } = router.query;
  const [sendTime, setSendTime] = useState();
  const [activeElements, setActiveElements] = useState<boolean[]>(
    props.data.map(() => false)
  );

  return (
    <div className={s.right_window}>
      <span className={s.right_window_title}>Пост в группу:</span>
      <div className={s.social_network_elements_wrapper}>
        {props.data.length === 0 && (
          <span
            className={s.active_btn}
            onClick={() => router.push(`/project/${projectName}/`)}
          >
            add social networks
          </span>
        )}
        {props.data.map((el, ind) => (
          <div className={s.social_network_element_wrapper}>
            <div
              key={ind}
              className={s.social_network_element}
              onClick={() =>
                setActiveElements(
                  activeElements.map((e, i) => (i === ind ? !e : e))
                )
              }
            >
              <div
                className={s.social_network_logo}
                style={activeElements[ind] ? {} : { background: "gray" }}
              >
                <Image
                  alt={"social network login"}
                  src={`/images/${el.logo}_logo_white.svg`}
                  height={30}
                  width={30}
                />
              </div>
              <span
                className={s.social_network_text}
                style={activeElements[ind] ? {} : { color: "gray" }}
              >
                {el.title}
              </span>
            </div>
          </div>
        ))}
      </div>
      <div className={s.time_dialog}>
        <span className={s.time_title}>Дата публикации:</span>
        <span className={s.time_body}>{sendTime ? sendTime : "now"}</span>
        <span className={s.active_btn}>Изменить время публикации</span>
      </div>
    </div>
  );
};

export default NetworkSelector;
