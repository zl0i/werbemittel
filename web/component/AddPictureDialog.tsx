import { BaseSyntheticEvent, FC, useEffect, useRef } from "react";
import { observer } from "mobx-react-lite";
import Image from "next/image";

import s from "../styles/addPictureDialog.module.css";

import newPostStore from "../store/newPostStore";

function preventAll(e: Event) {
  e.preventDefault();
  e.stopPropagation();
}

function loadPictureByDrop(e: DragEvent) {
  e.preventDefault();
  e.stopPropagation();
  if (e.dataTransfer?.files[0].type.includes("image")) {
    addPicture(e.dataTransfer.files[0]);
    e.dataTransfer.clearData();
    return;
  }
  alert("accept only PNG, JPG, SVG");
}

function loadPictureByBtn(e: BaseSyntheticEvent) {
  if (e.target.files[0]?.type.includes("image")) {
    addPicture(e.target.files[0]);
    return;
  }
  alert("accept only PNG, JPG, SVG");
}

function addPicture(image: File) {
  //TODO: send file to server
  console.log(image);
  newPostStore.setShowAddPictureDialog(false);
  newPostStore.addPicture(image);
}

const AddPictureDialog: FC = () => {
  const inputFileRef = useRef<HTMLInputElement | null>(null);
  const dropRef = useRef<HTMLInputElement | null>(null);

  useEffect(() => {
    if (dropRef.current) {
      const div: HTMLDivElement = dropRef.current;
      div.addEventListener("dragenter", preventAll);
      div.addEventListener("dragleave", preventAll);
      div.addEventListener("dragover", preventAll);
      div.addEventListener("drop", (image) => loadPictureByDrop(image));
    }

    return () => {
      if (dropRef.current) {
        const div: HTMLDivElement = dropRef.current;
        div.removeEventListener("dragenter", preventAll);
        div.removeEventListener("dragleave", preventAll);
        div.removeEventListener("dragover", preventAll);
        div.removeEventListener("drop", (image) => loadPictureByDrop(image));
      }
    };
  });

  return (
    <div
      className={s.smoke_wrap}
      onMouseDown={() => newPostStore.setShowAddPictureDialog(false)}
    >
      <div className={s.dialog} onMouseDown={(e) => e.stopPropagation()}>
        <div className={s.add_image_window} ref={dropRef}>
          drop here
        </div>
        <p style={{ fontSize: 30, margin: 15 }}>OR</p>
        <input
          type="file"
          ref={inputFileRef}
          style={{ display: "none" }}
          accept="image/*"
          onChange={(image) => loadPictureByBtn(image)}
        />
        <button
          className={s.add_image_btn}
          onClick={() => inputFileRef.current && inputFileRef.current.click()}
        >
          select file
        </button>
        <div
          className={s.add_picture_close_btn}
          onClick={() => newPostStore.setShowAddPictureDialog(false)}
        >
          <Image
            alt={"close"}
            src={`/images/close_btn.svg`}
            height={40}
            width={40}
          />
        </div>
      </div>
    </div>
  );
};

export default observer(AddPictureDialog);
