import { useRouter } from "next/router";
import Link from "next/link";
import { FC } from "react";

import s from "../styles/auth.module.css";

const AuthHeader: FC = () => {
  const router = useRouter();
  return (
    <div className={s.auth_header}>
      <div className={s.auth_header_bar}>
        <div
          className={`
          ${s.default_bar} 
          ${router.asPath === "/register" ? s.active_bar : s.inactive_bar}
            `}
        >
          <Link
            href={"/register"}
            replace={router.asPath === "/register" || router.asPath === "/login"}
          >
            <a>Регистрация</a>
          </Link>
          {router.asPath === "/register" && (
            <div className={s.active_bottom_line}>&nbsp;</div>
          )}
        </div>
      </div>
      <div className={s.auth_header_bar}>
        <div
          className={`
          ${s.default_bar} 
          ${router.asPath === "/login" ? s.active_bar : s.inactive_bar}`}
        >
          <Link
            href={"/login"}
            replace={router.asPath === "/register" || router.asPath === "/login"}
          >
            <a>Авторизация</a>
          </Link>
          {router.asPath === "/login" && (
            <div className={s.active_bottom_line}>&nbsp;</div>
          )}
        </div>
      </div>
    </div>
  );
};

export default AuthHeader;
