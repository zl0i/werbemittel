import { observer } from "mobx-react-lite";
import router, { useRouter } from "next/router";
import Link from "next/link";
import { FC } from "react";

import s from "../styles/appHeader.module.css";

import loginStore from "../store/loginStore";
import authAPI from "../api/authAPI";

function logoutBtn() {
  try {
    authAPI.logout();
    loginStore.removeUser();
    router.asPath === "/" ? router.reload() : router.replace("/");
  } catch (error) {
    console.log(error);
    alert("error");
  }
}

const AppHeader: FC = () => {
  const router = useRouter();

  return (
    <div className={s.header}>
      <div className={s.header_container}>
        <div className={s.left_bar}>
          <Link href={"/"}>
            <a className={s.logo}>Werbemittel</a>
          </Link>
          <a className={s.links} style={{ marginLeft: 54 }}>
            1 пункт
          </a>
          <a className={s.links} style={{ marginLeft: 44 }}>
            2 пункт
          </a>
          <a className={s.links} style={{ marginLeft: 44 }}>
            3 пункт
          </a>
        </div>
        <div className={s.right_bar}>
          {loginStore.getAccessToken() === "" ? (
            <>
              <Link
                href={"/register"}
                replace={
                  router.asPath === "/register" || router.asPath === "/login"
                }
              >
                <a className={s.reg_btn}>Зарегистрироваться</a>
              </Link>
              <Link
                href={"/login"}
                replace={
                  router.asPath === "/register" || router.asPath === "/login"
                }
              >
                <a className={s.login_btn}>Войти</a>
              </Link>
            </>
          ) : (
            <button
              className={`${s.login_btn} ${s.logout_btn}`}
              onClick={logoutBtn}
            >
              Log out
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default observer(AppHeader);
