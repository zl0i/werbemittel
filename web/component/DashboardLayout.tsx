import { FC, ReactNode, useState } from "react";
import { useRouter } from "next/router";

import s from "../styles/dashboardLayout.module.css";

const categoryMenu: { span: string; pageName: string; isBlocked?: boolean }[] =
  [
    { span: "соц.сети", pageName: "" },
    { span: "создать пост", pageName: "create" },
    { span: "история постов", pageName: "history", isBlocked: true },
    { span: "статистика", pageName: "statistic", isBlocked: true },
  ];

const DashboardLayout: FC<{ children: ReactNode }> = (props) => {
  const router = useRouter();
  const { projectName } = router.query;
  const [idActivePanel, setIdActivePanel] = useState<string>("");

  return (
    <div className={s.main_window}>
      <ul className={s.control_panel}>
        {categoryMenu.map((el, ind) => (
          <li
            key={ind}
            style={
              idActivePanel === el.span
                ? { background: "#C4C4C4" }
                : el.isBlocked
                ? { color: "gray" }
                : {}
            }
            title={el.isBlocked ? "Not working yet" : ""}
            onClick={() => {
              if (!el.isBlocked) {
                setIdActivePanel(el.pageName);
                router.push(`/project/${projectName}/${el.pageName}`);
              }
            }}
          >
            <span className={s.category_name}>{el.span}</span>
          </li>
        ))}
      </ul>
      <div className={s.active_panel}>
        <span>{projectName}</span>
        {props.children}
      </div>
    </div>
  );
};

export default DashboardLayout;
