import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import s from "../styles/loaderBar.module.css";

export default function LoaderBar() {
  const router = useRouter();
  const [isPageLoading, setPageLoading] = useState<boolean>(false);

  useEffect(() => {
    const handleStart = () => setPageLoading(true);
    const handleComplete = () => setPageLoading(false);

    router.events.on("routeChangeStart", handleStart);
    router.events.on("routeChangeComplete", handleComplete);
    router.events.on("routeChangeError", handleComplete);

    return () => {
      router.events.off("routeChangeStart", handleStart);
      router.events.off("routeChangeComplete", handleComplete);
      router.events.off("routeChangeError", handleComplete);
    };
  }, [router]);
  return (
    <>
      <div className={s.loadBar}/>
    </>
  );
}
