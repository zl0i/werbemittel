/** @type {import('next').NextConfig} */

const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");

module.exports = {
  reactStrictMode: true,
  webpackDevMiddleware: (config) => {
    config.watchOptions = {
      poll: 1000,
      aggregateTimeout: 300,
    };
    return config;
  },
  webpack(config, { dev }) {
    if (dev) {
      config.plugins.push(new ForkTsCheckerWebpackPlugin());
    }
    return config;
  },
};
