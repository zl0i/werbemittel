import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import router from "next/router";
import { NextPage } from "next";
import Image from "next/image";
import Link from "next/link";

import s from "../styles/auth.module.css";

import AuthHeader from "../component/AuthHeader";

import { authController } from "../src/authLogic";
import loginStore from "../store/loginStore";
import authAPI from "../api/authAPI";

function loginBtn(
  mail: string,
  setMailRed: (value: boolean) => void,
  pass: string,
  setPassRed: (value: boolean) => void
) {
  const authData = {
    mail,
    setMailRed,
    pass,
    setPassRed,
    sendTo: authAPI.loginByPassword,
  };
  authController(authData);
}

const Login: NextPage = () => {
  useEffect(() => {
    loginStore.getAccessToken() && router.push("/");
  }, []);

  const [pass, setPass] = useState<string>("");
  const [isMailRed, setMailRed] = useState<boolean>(false);
  const [isPassRed, setPassRed] = useState<boolean>(false);
  const [isShowPass, setShowPass] = useState<boolean>(false);
  return (
    <>
      <div className={s.auth_dialog}>
        <AuthHeader />
        <div className={s.input_container}>
          <input
            className={s.auth_input}
            placeholder={"Адрес электронной почты"}
            style={isMailRed ? { background: "#E06666" } : {}}
            value={loginStore.getMail()}
            onChange={(e) => {
              loginStore.setMail(e.target.value);
              isMailRed && setMailRed(false);
            }}
            onKeyPress={(e) =>
              e.key === "Enter" &&
              loginBtn(loginStore.getMail(), setMailRed, pass, setPassRed)
            }
          />
          <div style={{ position: "relative" }}>
            <input
              className={s.auth_input}
              placeholder={"Пароль"}
              style={isPassRed ? { background: "#E06666" } : {}}
              value={pass}
              onChange={(e) => {
                setPass(e.target.value);
                isPassRed && setPassRed(false);
              }}
              onKeyPress={(e) =>
                e.key === "Enter" &&
                loginBtn(loginStore.getMail(), setMailRed, pass, setPassRed)
              }
              type={isShowPass ? "text" : "password"}
            />
            <div
              style={{
                position: "absolute",
                right: 17,
                top: 40,
                height: 34,
                width: 70,
              }}
              onMouseDown={() => setShowPass(true)}
              onMouseUp={() => setShowPass(false)}
            >
              <div
                style={isShowPass ? { display: "block" } : { display: "none" }}
              >
                <Image
                  alt={"eyeShow"}
                  src={"/images/eye_show.svg"}
                  layout="fill"
                />
              </div>
              <div
                style={isShowPass ? { display: "none" } : { display: "block" }}
              >
                <Image
                  alt={"eyeHide"}
                  src={"/images/eye_hide.svg"}
                  layout="fill"
                />
              </div>
            </div>
          </div>
          <button
            className={s.auth_login_btn}
            disabled={!Boolean(loginStore.getMail()) || !Boolean(pass)}
            onClick={() =>
              loginBtn(loginStore.getMail(), setMailRed, pass, setPassRed)
            }
          >
            Войти
          </button>
          <Link href={"/login"}>
            <a className={s.forget_password_btn}>Я забыл пароль</a>
          </Link>
        </div>
      </div>
    </>
  );
};

export default observer(Login);
