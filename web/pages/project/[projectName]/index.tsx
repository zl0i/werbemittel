import { NextPage } from "next";
import Image from "next/image";

import DashboardLayout from "../../../component/DashboardLayout";
import s from "../../../styles/controlPage.module.css";

const socialNetworks: { title: string; isActive: boolean }[] = [
  { title: "vk", isActive: false },
  { title: "telegram", isActive: false },
  { title: "twitter", isActive: false },
  { title: "instagram", isActive: false },
  { title: "linkedin", isActive: false },
  { title: "youtube", isActive: false },
];

const Control: NextPage = () => {
  return (
    <>
      <DashboardLayout>
        <div className={s.active_zone}>
          <div>{/*//TODO add elements in left window */}</div>
          <div className={s.add_network_dialog}>
            <span className={s.dialog_title}>Добавить</span>
            {socialNetworks.map((el, ind) => (
              <div
                key={ind}
                className={s.network_btn}
                style={el.isActive ? {} : { background: "gray" }}
                title={el.isActive ? "" : "Not working yet"}
              >
                <Image
                  alt={"SN"}
                  src={`/images/${el.title}_logo_white.svg`}
                  width={32}
                  height={28}
                />
              </div>
            ))}
          </div>
        </div>
      </DashboardLayout>
    </>
  );
};

export default Control;
