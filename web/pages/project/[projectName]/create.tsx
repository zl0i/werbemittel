import type { GetServerSideProps, NextPage } from "next";
import { observer } from "mobx-react-lite";
import Image from "next/image";

import NetworkSelector from "../../../component/NetworkSelectorInCreate";
import AddPictureDialog from "../../../component/AddPictureDialog";
import DashboardLayout from "../../../component/DashboardLayout";
import s from "../../../styles/createPost.module.css";

import newPostStore from "../../../store/newPostStore";

export interface ISendTo {
  title: string;
  logo: string;
}

//TODO add project
const Create: NextPage<{ data: ISendTo[] }> = ({ data }) => {
  return (
    <DashboardLayout>
      <div className={s.container}>
        {newPostStore.isShowAddPictureDialog() && <AddPictureDialog />}
        <div className={s.left_window}>
          <textarea
            className={s.post_textarea}
            placeholder={"Введите текст поста"}
          />
          <div className={s.post_additional}>
            <div className={s.post_additional_left}>
              <p className={s.post_additional_title}>Изображения:</p>
              <div className={s.picture_flexbox}>
                {newPostStore.getPictures().map((e, i) => (
                  <div style={{ position: "relative" }} key={i}>
                    <Image
                      alt={"picture #" + i}
                      src={URL.createObjectURL(newPostStore.getPictures()[i])}
                      draggable={false}
                      width={55}
                      height={55}
                    />
                    <div className={s.delete_picture_btn}>
                      <Image
                        alt={"delete picture"}
                        src={"/images/post_delete_picture_btn.svg"}
                        layout={"fill"}
                        onClick={() => newPostStore.removePictureById(i)}
                      />
                    </div>
                  </div>
                ))}
                <div className={s.add_picture_btn}>
                  <Image
                    alt={"add picture"}
                    src={"/images/add_picture_btn.svg"}
                    layout={"fill"}
                    onClick={() =>
                      newPostStore.getPictures().length < 9
                        ? newPostStore.setShowAddPictureDialog(true)
                        : console.error("you can't add more than 9 pictures")
                    }
                  />
                </div>
              </div>
            </div>
          </div>
          <div className={s.footer_div}>
            <a className={s.send_post_btn}>Предпросмотр</a>
            <a className={s.send_post_btn}>Опубликовать</a>
          </div>
        </div>
        <NetworkSelector data={data} />
      </div>
    </DashboardLayout>
  );
};

export default observer(Create);

export const getServerSideProps: GetServerSideProps = async (res) => {
  const projectName = res.params?.projectName;
  const error = [{ title: "Error", logo: "error" }];
  if (!projectName && Array.isArray(projectName)) return { props: { error } };
  //TODO ↓ receive data by axios from API
  const data: ISendTo[] = [
    { title: "vkontak", logo: "vk" },
    { title: "ptica", logo: "twitter" },
    { title: "mygop", logo: "youtube" },
  ];
  return {
    props: { data },
  };
};
