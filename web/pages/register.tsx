import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import router from "next/router";
import { NextPage } from "next";
import Image from "next/image";

import s from "../styles/auth.module.css";

import AuthHeader from "../component/AuthHeader";

import { authController } from "../src/authLogic";
import loginStore from "../store/loginStore";
import authAPI from "../api/authAPI";

function makeRegBtn(
  mail: string,
  setMailRed: (value: boolean) => void,
  pass: string,
  rePass: string,
  setPassRed: (value: boolean) => void
) {
  if (pass !== rePass) {
    alert("pass / rePass don't match");
    setPassRed(true);
    return;
  }
  const authData = {
    mail,
    setMailRed,
    pass,
    setPassRed,
    sendTo: authAPI.registerByPassword,
  };
  authController(authData);
}

const Register: NextPage = () => {
  useEffect(() => {
    loginStore.getAccessToken() && router.push("/");
  }, []);

  const [isMailRed, setMailRed] = useState<boolean>(false);
  const [pass, setPass] = useState<string>("");
  const [rePass, setRePass] = useState<string>("");
  const [isPassRed, setPassRed] = useState<boolean>(false);
  const [validate, setValidate] = useState<boolean>(false);
  const [isShowPass, setShowPass] = useState<boolean>(false);
  return (
    <>
      <div className={s.auth_dialog}>
        <AuthHeader />
        <div className={s.input_container}>
          <input
            className={s.auth_input}
            placeholder={"Адрес электронной почты"}
            style={isMailRed ? { background: "#E06666" } : {}}
            value={loginStore.getMail()}
            onChange={(e) => {
              loginStore.setMail(e.target.value);
              isMailRed && setMailRed(false);
            }}
            onKeyPress={(e) =>
              e.key === "Enter" &&
              makeRegBtn(
                loginStore.getMail(),
                setMailRed,
                pass,
                rePass,
                setPassRed
              )
            }
          />
          <div style={{ position: "relative" }}>
            <input
              className={s.auth_input}
              placeholder={"Пароль"}
              style={isPassRed ? { background: "#E06666" } : {}}
              value={pass}
              onChange={(e) => {
                setPass(e.target.value);
                isPassRed && setPassRed(false);
              }}
              onKeyPress={(e) =>
                e.key === "Enter" &&
                makeRegBtn(
                  loginStore.getMail(),
                  setMailRed,
                  pass,
                  rePass,
                  setPassRed
                )
              }
              type={isShowPass ? "text" : "password"}
            />
            <div
              style={{
                position: "absolute",
                right: 17,
                top: 40,
                height: 34,
                width: 70,
              }}
              onMouseDown={() => setShowPass(true)}
              onMouseUp={() => setShowPass(false)}
            >
              <div
                style={isShowPass ? { display: "block" } : { display: "none" }}
              >
                <Image
                  alt={"eyeShow"}
                  src={"/images/eye_show.svg"}
                  layout="fill"
                />
              </div>
              <div
                style={isShowPass ? { display: "none" } : { display: "block" }}
              >
                <Image
                  alt={"eyeHide"}
                  src={"/images/eye_hide.svg"}
                  layout="fill"
                />
              </div>
            </div>
          </div>
          <input
            className={s.auth_input}
            placeholder={"Подтвердите пароль"}
            style={isPassRed ? { background: "#E06666" } : {}}
            value={rePass}
            onChange={(e) => {
              setRePass(e.target.value);
              isPassRed && setPassRed(false);
            }}
            onKeyPress={(e) =>
              e.key === "Enter" &&
              makeRegBtn(
                loginStore.getMail(),
                setMailRed,
                pass,
                rePass,
                setPassRed
              )
            }
            type={isShowPass ? "text" : "password"}
          />
        </div>
        <div className={s.auth_footer}>
          <div className={s.checkbox_container}>
            <input
              type={"checkbox"}
              className={s.checkbox}
              checked={validate}
              onChange={() => setValidate(!validate)}
            />
            <p
              className={s.checkbox_text}
              onClick={() => setValidate(!validate)}
            >
              Даю согласие на обработку данных
            </p>
          </div>
          <button
            className={s.auth_reg_btn}
            style={validate ? {} : { background: "grey", color: "#CCCCCC" }}
            disabled={!validate}
            onClick={() =>
              makeRegBtn(
                loginStore.getMail(),
                setMailRed,
                pass,
                rePass,
                setPassRed
              )
            }
          >
            Зарегистрироваться
          </button>
        </div>
      </div>
    </>
  );
};

export default observer(Register);
