import type { NextPage } from "next";
import Image from "next/image";
import Link from "next/link";

import s from "../styles/about.module.css";

const About: NextPage = () => {
  return (
    <>
      <div className={"container"}>
        <div className={s.left_window}>
          <p className={s.text_title}>Заголовок</p>
          <p className={s.text_welcome}>
            Сайт рыбатекст поможет дизайнеру, верстальщику, <br /> вебмастеру
            сгенерировать несколько абзацев более менее осмысленного текста рыбы
            на русском языке, а начинающему оратору отточить навык публичных
            выступлений в домашних условиях.
          </p>
          <Link href={"/register"}>
            <a className={s.demo_btn}>Попробовать бесплатно</a>
          </Link>
        </div>
        <div className={s.right_window}>
          <Image
            alt={"empty_img"}
            src="/images/empty_image.svg"
            width={540}
            height={483}
          />
        </div>
      </div>
    </>
  );
};

export default About;
