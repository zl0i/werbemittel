import NextProgress from "next-progress";
import type { AppProps } from "next/app";
import { useEffect } from "react";
import Head from "next/head";

import "../styles/globals.css";

import AppHeader from "../component/AppHeader";

import { setNewAccessTokens } from "../src/authLogic";
import loginStore from "../store/loginStore";

export default function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    const accessTokenLocal: string | null = localStorage.getItem("accessToken");
    if (!loginStore.getAccessToken() && accessTokenLocal) {
      loginStore.setUser(accessTokenLocal);
    }
    if (!loginStore.getAccessToken() && !accessTokenLocal) {
      setNewAccessTokens();
    }
  }, []);

  return (
    <>
      <Head>
        <title>Werbemittel</title>
        <meta name="description" content="Need description" />
        <link rel="icon" href="images/favicon.ico" />
      </Head>
      <NextProgress
        delay={50}
        height={"7px"}
        options={{ showSpinner: false }}
      />
      <AppHeader />
      <Component {...pageProps} />
    </>
  );
}
