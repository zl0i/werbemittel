import { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import type { NextPage } from "next";
import router from "next/router";
import Image from "next/image";

import projectsStore from "../store/projectsStore";
import s from "../styles/index.module.css";

function addProjectBtn() {
  const newProject = {
    title: "Новый проект",
  };
  projectsStore.addProject(newProject);
  router.push("/project/new_project");
}

const Home: NextPage = () => {
  useEffect(() => {
    if (!localStorage.getItem("accessToken")) {
      router.replace("/about");
      return;
    }
    //TODO ↓ receive data by axios from API
    const data = [
      { title: "Автомобили", socialNetworkUse: ["vk", "twitter"] },
      {
        title: "Мебель из красного дерева",
        socialNetworkUse: ["telegram", "youtube"],
      },
      { title: "Авиазакрылки", socialNetworkUse: [] },
    ];
    projectsStore.loadAllProjects(data);
    setShowUl(true);
  }, []);

  //TODO isshowUL ???
  const [isShowUl, setShowUl] = useState<boolean>(false);
  return (
    <div className={"container"}>
      <ul
        style={{ display: isShowUl ? "flex" : "none" }}
        className={s.dashboard}
      >
        <li
          className={s.add_project}
          style={{ justifyContent: "center" }}
          onClick={addProjectBtn}
        >
          <Image
            alt={"add project"}
            src="/images/plus_in_box.svg"
            width={48}
            height={48}
          />
          <span>Создать проект</span>
        </li>
        {projectsStore.getProjects().map((el, ind) => (
          <li
            key={ind}
            onClick={() => {
              projectsStore.setSelectedProject(el);
              router.push(`/project/${el.title}`);
            }}
          >
            <span className={s.element_title}>{el.title}</span>
            <div className={s.element_footer}>
              {el.socialNetworkUse?.map((name, pos) => (
                <Image
                  style={{ background: "#C4C4C4" }}
                  key={pos}
                  alt={"SN"}
                  src={`/images/${name}_logo_white.svg`}
                  width={26}
                  height={26}
                />
              ))}
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default observer(Home);
