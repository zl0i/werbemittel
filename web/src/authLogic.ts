import axios, { AxiosResponse } from "axios";
import authAPI from "../api/authAPI";
import router from "next/router";

import loginStore from "../store/loginStore";

export function authValidate(
  mail: string,
  pass: string,
  setMailRed: (value: boolean) => void,
  setPassRed: (value: boolean) => void
) {
  const isValidate = Boolean(
    mail
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
  );
  if (mail.length === 0) {
    setMailRed(true);
    throw new Error("incorrect mail");
  }
  if (!isValidate) {
    setMailRed(true);
    throw new Error("incorrect mail");
  }
  if (pass.length < 6) {
    setPassRed(true);
    throw new Error("short password");
  }
}

interface IAuthData {
  mail: string;
  setMailRed: (value: boolean) => void;
  pass: string;
  setPassRed: (value: boolean) => void;
  sendTo: (login: string, password: string) => Promise<AxiosResponse<any, any>>;
}

export async function authController(authData: IAuthData) {
  try {
    authValidate(
      authData.mail,
      authData.pass,
      authData.setMailRed,
      authData.setPassRed
    );
    const { data } = await authData.sendTo(authData.mail, authData.pass);
    loginStore.setUser(data.accessToken);
    alert("success");
    router.replace("/");
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      const response: AxiosResponse = error.response;
      alert(response.data.message);
    } else if (error instanceof Error) {
      alert(error.message);
    } else {
      alert("unknown error");
    }
  }
}

export async function setNewAccessTokens() {
  try {
    await authAPI.findRefreshToken();
    const { data } = await authAPI.refreshToken();
    loginStore.setUser(data.accessToken);
  } catch (error) {
    loginStore.removeUser();
    await authAPI.logout();
    console.error(error);
  }
}
