export function getMonthsName(number: number): string {
  const rusMonth: string[] = [
    "",
    "января,",
    "февраля,",
    "марта,",
    "апреля,",
    "мая,",
    "июня,",
    "июля,",
    "августа,",
    "сентября,",
    "октября,",
    "ноября,",
    "декабря,",
  ];
  return number < 1 || number > 12 ? "unknown month" : rusMonth[number];
}
