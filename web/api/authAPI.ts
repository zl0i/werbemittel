import axios from "axios";

export default class authAPI {
  static async loginByPassword(login: string, password: string) {
    return await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/auth/signin`,
      {
        login: login,
        password: password,
      },
      {
        withCredentials: true,
      }
    );
  }

  static loginByOAuth(method: string) {
    window.open(
      `${process.env.NEXT_PUBLIC_API_AUTH_URL}/oauth?method=${method}&device=web`,
      "",
      "width=700,height=500,left=200,top=200"
    );
  }

  static forgetPassword() {
    return undefined;
  }

  static async registerByPassword(login: string, password: string) {
    return await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/auth/signup`, {
      login: login,
      password: password,
    });
  }

  static async findRefreshToken() {
    return await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/auth/findrefreshtoken`,
      {},
      {
        withCredentials: true,
      }
    );
  }

  static async refreshToken() {
    return await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/auth/refresh`,
      {},
      {
        withCredentials: true,
      }
    );
  }

  static async logout() {
    return await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/auth/logout`,
      {},
      {
        withCredentials: true,
      }
    );
  }
}
